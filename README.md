# Diritto alla privacy e riservatezza_Our Awesome Hardware Project

E' facile sentirsi protetti con dello scotch su una webcam.
E' altrettanto facile distruggere o disassemblare un microfono, da qualsiasi tipo di dispositivo, senza compromettere le funzioni di input audio.